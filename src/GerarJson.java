import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;

public class GerarJson {

	public static void main(String[] args) {

		Cerveja skol = new Cerveja();
		skol.setAno("2019");
		skol.setMalte("Puro malte");
		skol.setMarca("Skol");
		skol.setNome("Skol Ice Blue Red");

		Gson g = new Gson();
		String json = g.toJson(skol);
		

		try {
			FileWriter writer = new FileWriter("Cerveja.json");

			writer.write(json);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(json);
	}

}
